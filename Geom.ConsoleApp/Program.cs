﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geom.Core;

namespace Geom.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var world = new World(1, 10);

            world.Start(10, 3);

            Console.ReadKey();
        }
    }
}
