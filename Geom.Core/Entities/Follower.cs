﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using Geom.Core.Extensions;

namespace Geom.Core.Entities
{
    /// <summary>
    /// Последователь. Тысячи их.
    /// У каждого есть три параметра, влияющих на приверженность к твоей или к чужой религии.
    /// </summary>
    public class Follower
    {
        /// <summary>
        /// Имя последователя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Парметры его религиозности
        /// </summary>
        public Religion Religion { get; set; }

        /// <summary>
        /// Координаты в этом бренном мире
        /// </summary>
        public Point Coordinates { get; set; }

        protected Point EndPoint { get; set; }

        /// <summary>
        /// Скорость в тайлах за ход
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Максимальное расстояние, на которое может перейти персонаж за одно действие
        /// </summary>
        public double MaxDistance { get; set; }

        /// <summary>
        /// Очередь действий последователя.
        /// Например, он может идти куда-нибудь. А может не идти.
        /// </summary>
        protected readonly Queue<Expression<Action>> ActionQueue = new Queue<Expression<Action>>();

        public Follower(int i)
        {
            Name = i.ToString();
            Speed = 4;
            Coordinates = new Point(i * Speed, i * Speed);
            MaxDistance = 10;
        }

        /// <summary>
        /// Постепенно идет к цели. Будем считать, что кратчайшим путем, то есть, по прямой.
        /// </summary>
        /// <param name="coordinates"></param>
        public void GoTo(Point coordinates)
        {
            var distance = MathService.GetDistance(Coordinates, coordinates);

            if (!DistanceIsNear(distance))
            {
                EndPoint = Coordinates;
                return;
            }

            EndPoint = coordinates;

            // Чувак идет к месту по прямой с учетом его скорости.
            if (Coordinates.X > coordinates.X)
            {
                if (Coordinates.X - coordinates.X > Speed)
                {
                    Coordinates.Offset(-Speed, 0);
                }
                else
                {
                    Coordinates.Offset(coordinates.X - Coordinates.X, 0);
                }
            }
            else
            {
                if (coordinates.X - Coordinates.X > Speed)
                {
                    Coordinates.Offset(Speed, 0);
                }
                else
                {
                    Coordinates.Offset(Coordinates.X - coordinates.X, 0);
                }
            }

            if (Coordinates.Y > coordinates.Y)
            {
                if (Coordinates.Y - coordinates.Y > Speed)
                {
                    Coordinates.Offset(0, -Speed);
                }
                else
                {
                    Coordinates.Offset(0, coordinates.Y - Coordinates.Y);
                }
            }
            else
            {
                if (coordinates.Y - Coordinates.Y > Speed)
                {
                    Coordinates.Offset(0, Speed);
                }
                else
                {
                    Coordinates.Offset(0, Coordinates.Y - coordinates.Y);
                }
            }

            if (Coordinates != coordinates)
            {
                ActionQueue.Enqueue(() => GoTo(coordinates));
            }
            else
            {
                EndPoint = Coordinates;
            }
        }

        /// <summary>
        /// Проверяет, близко ли находится пункт назначения или далеко
        /// Если далеко, то никто никуда не пойдет
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        protected virtual bool DistanceIsNear(double distance)
        {
            return distance/Speed <= MaxDistance;
        }

        /// <summary>
        /// Просто пробегаемся по очереди и делаем то, что в ней написано
        /// </summary>
        public void MakeTurn()
        {
            // Если есть что-то в очереди, то выполняем это.
            // Если в очереди ничего нет, то просто перемещаемся в каком-нибудь направлении

            if (ActionQueue.Count != 0)
            {
                ActionQueue.Dequeue().Compile().Invoke();
            }
            else
            {
                ActionQueue.Enqueue(() => this.GoTo(GetRandomCoordinates()));   
            }
            Console.WriteLine(ToString());
        }

        private Point GetRandomCoordinates()
        {
            var random = new Random();

            var number = random.NextDouble();

            if (number < 0.25)
            {
                return new Point(Coordinates.X - Speed * 2, Coordinates.Y);
            }
            if (number < 0.5)
            {
                return new Point(Coordinates.X, Coordinates.Y - Speed * 2);
            }
            if (number < 0.75)
            {
                return new Point(Coordinates.X, Coordinates.Y + Speed * 2);
            }
            return new Point(Coordinates.X + Speed * 2, Coordinates.Y);
        }
        
        public override string ToString()
        {
            return string.Format("Follower {0} is on ({1}:{2}). Follower is go to ({3}:{4})", Name, Coordinates.X, Coordinates.Y, EndPoint.X, EndPoint.Y);
        }
    }
}