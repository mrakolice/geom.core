﻿using System;
using System.Drawing;
using System.Linq.Expressions;
using System.Reflection;
using Geom.Core.Extensions;

namespace Geom.Core.Entities
{
    /// <summary>
    /// Пророк. Имеет разные параметры религиозности и такой параметр, как харизма.
    /// Благодаря этому параметру может убеждать остальных последователей обратиться в твою веру
    /// </summary>
    public class Prophet:Follower
    {
        /// <summary>
        /// Харизма. Сила убеждения.
        /// </summary>
        public double Charisma { get; set; }

        public override string ToString()
        {
            return string.Format("Prophet {0} is on ({1}:{2}). Prophet is go to ({3}:{4})", Name, Coordinates.X, Coordinates.Y, EndPoint.X, EndPoint.Y);
        }

        public Prophet(int i) : base(i)
        {
        }
    }
}