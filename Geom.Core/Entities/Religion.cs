﻿namespace Geom.Core.Entities
{
    public class Religion
    {
        /// <summary>
        /// Фанатичность
        /// </summary>
        public double Fanaticism { get; set; }

        /// <summary>
        /// Осознанность
        /// </summary>
        public double Awareness { get; set; }

        /// <summary>
        /// Святость
        /// </summary>
        public double Holiness { get; set; } 
    }
}