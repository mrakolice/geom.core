﻿using System.Drawing;

namespace Geom.Core.Entities
{
    /// <summary>
    /// Объект храма
    /// </summary>
    public class Temple
    {
        /// <summary>
        /// Параметры религии храма
        /// </summary>
        public Religion Religion { get; set; }

        /// <summary>
        /// Координаты в этом бренном мире
        /// </summary>
        public Point Coordinates { get; set; }
    }
}