﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Geom.Core.Entities;

namespace Geom.Core
{
    /// <summary>
    /// Мир представляет из себя двумерную сетку.
    /// </summary>
    public class World
    {
        /// <summary>
        /// В принципе, все последователи, которые есть в мире.
        /// </summary>
        public List<Follower> Followers { get; set; }

        /// <summary>
        /// Возвращает список всех пророков
        /// </summary>
        public List<Prophet> Prophets { get { return Followers.OfType<Prophet>().ToList(); }}

        /// <summary>
        /// Количество ходов в секунду
        /// </summary>
        public int TurnCount { get; set; }

        /// <summary>
        /// Количество ходов до конца
        /// </summary>
        public int TurnEnd { get; set; }

        public World(int turnCount, int turnEnd)
        {
            Followers = new List<Follower>();

            TurnCount = turnCount;
            TurnEnd = turnEnd;
        }

        public void Start(int followerCount, int prophetCount)
        {
            // Генерируем мир (последователей и пророков)

            for (int j = 0; j < prophetCount; j++)
            {
                Followers.Add(new Prophet(j));
            }

            for (int i = 0; i < followerCount-prophetCount; i++)
            {
                Followers.Add(new Follower(i));
            }

            // Делаем ходы

            for (int i = TurnEnd; i > 0; i--)
            {
                Thread.Sleep(1000 / TurnCount);

                Console.WriteLine("Turn {0}", TurnEnd - i + 1);
                MakeTurn();   
                Console.WriteLine("Turn was made. Remains {0} turns.", i - 1);
            }
        }
        
        /// <summary>
        /// Делает ход. Просчитывает все параметры и тд
        /// </summary>
        public void MakeTurn()
        {
            Followers.ForEach(f => f.MakeTurn());
        }
    }
}